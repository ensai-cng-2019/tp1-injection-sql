import sys
from dao import Dao

def main():
    repo = Dao()
    repo.execute(sys.argv[1])

if __name__ == "__main__":
    main()