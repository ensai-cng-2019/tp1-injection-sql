from connection import get_connection

class Dao:
    def execute(self, name):
        """
        List content of table name in parameter
        """

        self.connection = get_connection()
        with self.connection.cursor() as cur:
            sql = "select * from %s" % (name,)
            cur.execute(sql)

            # Voici comment se protéger d'une injection SQL avec psycopg2 : passer les paramètres à la méthode execute
            # qui s'occupe de faire la conversion appropriée
            # cur.execute("select * from %s", (name,))
            self.connection.commit()
            for item in cur.fetchall():
                print(item)