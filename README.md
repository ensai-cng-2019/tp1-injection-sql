Exemple d'appel normal
```
python main.py "avengers"
```

Exemple d'appel avec injection SQL
```
python main.py "agents;create table test (id serial)"
```
Le caractère ';' permet d'échapper la requête SQL et d'ajouter une requête malicieuse